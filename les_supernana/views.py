from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from .forms import ContactForm
from .mail import  send_contactmessage


@login_required
def view_contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            form_name = form.cleaned_data['name']
            form_email = form.cleaned_data['email']
            form_message = form.cleaned_data['message']
            send_contactmessage(name=form_name, message=form_message, email=form_email)
    else:
        form = ContactForm()
    return render(request, "supernana/contact.html", {'form': form})
