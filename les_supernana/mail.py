from django.core.mail import send_mail
from django.conf import settings


def send_contactmessage(name, email, message):
    subject = 'Hello %s' % name
    message = 'You send the message : %s' % message
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [email,]

    send_mail( subject, message, email_from, recipient_list )